### Instructions on Generating .xodr & .fbx files from .osm
##### 

1) put .osm file to convert into the input folder
2) run command:
```bash
./osmToCarlaMap.sh osmFilename
```
- note: 
	- Make sure to just pass the name in as a parameter, not the .osm file extension as well. If you pass in any other filetype it will not work.
