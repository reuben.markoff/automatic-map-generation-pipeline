#!/bin/bash

# mkdir to store output in
mkdir out/$1

# generate .xodr from .osm file
cp input/$1.osm programs/osm2xodr
cd programs/osm2xodr
python3 main.py $1.osm
rm $1.osm
mv XODR_files/$1.xodr ../../out/$1

# generate .obj from .osm file
cd ../..
cp input/$1.osm programs/"OSM2World-0.2.0-bin"
cd programs/"OSM2World-0.2.0-bin"
source osm2world.sh -i $1.osm -o $1.obj
mv $1.obj ../"blender-3.0.0-linux-x64"/obj_files
mv $1.obj.mtl ../"blender-3.0.0-linux-x64"/obj_files
rm $1.osm

# generate .fbx from .obj file
cd ../"blender-3.0.0-linux-x64"
blender -b -P objToFbx.py -- $1
mv fbx_files/$1.fbx ../../out/$1
rm obj_files/$1.obj
rm obj_files/$1.obj.mtl

