import bpy
import sys

# delete original cube in blender project
bpy.ops.object.delete()

# import obj
bpy.ops.import_scene.obj(filepath="obj_files/" + sys.argv[5] + ".obj", axis_forward='-Z', axis_up='Y')

# export fbx
bpy.ops.export_scene.fbx(filepath="fbx_files/" + sys.argv[5] + ".fbx", axis_forward='-Z', axis_up='Y')

# quit blender
bpy.ops.wm.quit_blender()